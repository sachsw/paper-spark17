When users interact with computers, their actions may unwittingly
cause undesirable effects due to unanticipated consequences. We
propose a monitoring process to identify ``trigger actions'', which are
user actions that may ultimately lead to states that the user views as
undesirable. The identification is complicated by three factors: (1)
the need to be judicious in interrupting user action, (2) noisy
observations (i.e., extraneous actions) and (3) partial observability
(i.e., missing actions). At the core of the proposed trigger action
identification process is a multi-objective evaluation function, which
assesses the ``criticality'' of each observation based on three
objectives: certainty (likelihood that the action is part of some plan
that leads to an undesirable state), timeliness (early recognition of
potential for danger) and action desirability (a measure of either the
commonality of the action or of its contribution to the user's
intended goals, if known). To this end, a ``critical trigger action" is
identified as one that maximizes a linear combination of all three
objectives. Our monitoring approach leverages ideas from plan
recognition by mapping observed actions to possible plans that have
some undesirable states as goals. We then apply the objective function
to decide whether or not to flag an observation as critical. Performance is evaluated in terms of two accuracy metrics:
success rate in ignoring extraneous actions and success rate in flagging undesirable actions compared to a ground truth, on controlled data sets (observation traces and PDDL domains) derived from the plan recognition literature and data from a human subject study of computer security. Additionally, we measure computational overhead by CPU time. To expedite the experimentation, we present an algorithm to generate observation traces with controlled levels of extraneous and missing actions to assess the impact of noise and partial observability.