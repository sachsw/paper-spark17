\subsection{Objective Function} 
\label{sec:objectivefunction}
Our focus is on identifying the intervention point as early as is \textit{helpful} when a user may be triggering an undesirable state. The concept of helpful is similar to the value of alerts described in \cite{Wilkins2003} which recognized that it is important to be judicious in interrupting a user in scenarios where humans and machine agents interact with each other: don't ignore but don't annoy. To this end, we start by defining a multi-objective function composed of three objective metrics to quantify the notion of helpful: certainty, timeliness and desirability.

When producing the set of alternative undesirable plans $\Pi_u$, as representative of a set of possible trajectories, for a specific undesirable goal, it would be best to consider alternatives that are non-obvious and diverse from each other rather than just the optimal plan.  This is especially important in long-running processes with complex traces or when a suboptimal plan contains a longer, but perhaps more easy to trigger, path to the undesirable state. {\it Certainty} measures the likelihood of action $a$ occurring in $\Pi_u$.
\begin{equation}
Certainty (a|\Pi_u)  = \frac{|\pi_u \in \Pi_u \textup{ in which}\; a\; \textup{occurs}|}{|\Pi_u|}
\end{equation}
%now justify why i think certainty is a good metric and how using alternative plans supports computation of this metric
The intuition behind the certainty metric is that, given a set of alternative plans, actions occurring frequently in those plans are indicative of the importance of that action toward causing some undesirable state. For example, if an action $a$ occurs in all plans in the set of alternative plans, the certainty metric will assign a high value to $a$, giving it a higher probability of being selected as a critical trigger compared to a less frequent action. Using a set of alternative plans supports a form of sampling that allows us to quantify this notion with an easily computable metric.

%justification to timeliness
Similarly, {\it timeliness} requires knowing what actions could yet be observed, which might effect the causation of the undesirable state. For this study, timeliness is measured by the maximum normalized steps remaining in the set of undesirable plans $\Pi_u$ in which the action a occurs. Let $n$ be the number of steps in $\pi_u$ going forward from $a$,
\begin{equation}
Timeliness (a|\Pi_u) = \max_{\pi_u \in \Pi_u}\left(\frac{
	\max\left( n \right)\
	}{\; |\pi_u|}\right) 
\end{equation}
Our definition of timeliness quantifies how close in discrete time an observation is to triggering the undesirable state within an undesirable plan in which it appears. Ideally, we would like the critical trigger action have a high timeliness score, because it indicates that an undesirable plan is developing but not imminent. This allows early recognition and provides more time to intervene if needed. Low timeliness score indicates undesirable state is imminent, resulting in a state where recovery is impossible.

%justification for desirability
Neither metric captures application specific information that acknowledge the role of the action in furthering the user's actual goals. {\it Desirability} measures the effect of the action on user's goals. Ideally, such a measure helps separate common harmless actions (e.g, user opening web browser) from avoidable ones and connects the observations to knowledge of the user's goals. For this study, we use the measure to downgrade the contribution to criticality of common actions, a general and fairly easily computed metric. Thus desirability is defined as:
\begin{equation}
Desirability (a|\Pi_u) = - \frac{|\textup{appearance of}\; a\; \textup{ in } \Pi_u|}{\sum_{i=1}^{|\Pi_u|}\left | \pi_i \right |}
\end{equation}
Combining the three metrics, we derive a critical trigger multi-objective function ($V(a)$) for candidate trigger action $a$ (where $c_i$ is the $a$ that the maximizes the value of $V$) where $\alpha_1, \alpha_2, \alpha_3$ are weights assigned to each metric:
\[
\begin{array}{cl}
V(a) = & \alpha_1 * Certainty (a|\Pi_u) + \alpha_2 * Timeliness (a|\Pi_u)\\
		 & - \alpha_3 * Desirability (a|\Pi_u)\\
\end{array}
\]

\debug{SACH}{from icaps: \textbf{For the timeliness objective function, I’m confused at why is “max” used, instead of using “min”. Let’s say you have already generated a pool of plan traces, and meanwhile an agent is executing one of them. Therefore the safest thing is to consider the timeliness based on the minimal distance from the triggering action, to the goal, so that you have a lower bound. So if you have two sets of plan traces (set1 according to action\_a, and set2 action\_b), and both sets contain 10 plan traces. We also assume that the maximum\-length plan in set1 has a length 20, that is larger than the maximum length one in set2, which has a length 18. But for set1, the length of all rest 9 plan traces are smaller than 5, whereas the length of all rest plans in set2 ranges from 14 to 16 (each of these plan contains 14\-16 actions). So based on your objective function, for the inner max, action\_a might be the 19th action in a plan (the n in formula (2) is 1), whereas action\_a in other!
	plans (lengths are 5) are 1st action (n is 4), so one of other plans (lengths are 5), but not the plan whose length is 20, would be chosen. For the outer max, lets say all plans in set2, for action\_b, have length 18, but it’s only because that in set1 there is a 20-length plan (even if the rest plans in set1 are super short), that the action\_a would be chosen, but not the action\_b.}
I don't understand this argument. we are do not have different sets of plans for each action in the observation trace. we only have a sample of 10 plans to reach undesirable state from the current state. When you move from one observation to the next, the sample of plans is updated to reflect the change of state resulting from the observation we just made. Maybe our explanation is not clear enough to convey that?
	}
\debug{SACH}{from icaps: \textit{\textbf{Furthermore, I am not completely convinced that these metrics (as a linear combination) can capture a critical trigger action. The objectives “certainty” and “desirability” are more or less capturing the same idea, both are capturing how many times an action appears either in terms of count of plans or in terms of plan lengths. Also, the objective weights provided in the setting, seem to be manually provided. Why did the authors settle on those particular values among myriads of different combinations. It would be good to get a justification about the chosen objective weights. Such weights can also be computed (or “learned”) using some machine learning techniques. Also apart from linear combination, were any non linear combinations tried out?}}}

\begin{table}[htbp]
	\centering
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{|l|l|l|l|l|}
			\hline
			& Certainty & Timeliness & Desirability & $V(a)$ \\
			\hline
			b & 2/10=0.2 & max(3/3,4/4)=1.0 & -2/39=0.05 & 0.38 \\
			c & 4/10=0.4 & max(2/2,3/3,5/5,6/6)=1.0 & -4/39=0.1 & 0.43 \\
			d & 4/10=0.4 & max(3/5,4/6,3/5,4/6)=0.6 & -4/39=0.1 & 0.30 \\
			f & 6/10=0.6 & max(2/5,3/6,2/2,3/3,5/5,6/6)=1.0 & -6/39=0.15 & 0.48 \\
			g & 2/10=0.2 & max(2/3,3/4)=0.75 & -2/39=0.05 & 0.30 \\
			h & 1/10=0.1 & max(1/2)=0.5 & -1/39=0.03 & 0.19 \\
			j & 3/10=0.3 & max(2/3,4/5,5/6)=0.83 & -3/39=0.08 & 0.35 \\
			k & 4/10=0.4 & max(2/6,2/3,4/5,5/6)=0.83 & -6/39=0.15 & 0.36 \\
			l & 3/10=0.3 & max(1/5,1/2,1/5)=0.5 & -3/39=0.08 & 0.24 \\
			m & 1/10=0.1 & max(2/4)=0.5 & -1/39=0.03 & 0.19\\
			n & 1/10=0.1 & max(1/3)=0.33 & -1/39=0.03 & 0.13 \\
			p & 4/10=0.4 & max(1/3,1/6,1/3,1/6)=0.33 & -4/39=0.1 & 0.21 \\
			q & 1/10=0.1 & max(1/4)=0.25 & -1/39=0.03 & 0.11 \\
			\hline    
		\end{tabular}
	}%
	\caption{Certainty, timeliness, desirability computations for each candidate trigger action in a synthetic example. $V(a)$ is the value returned by the critical trigger multi-objective function assuming equal weighting (i.e., $\alpha=0.33$) for each candidate trigger action}
	\label{tab:metrics}
\end{table}
Table \ref{tab:metrics}, shows how the critical trigger action is identified using the proposed objective function for the example in figure \ref{fig:problem}. At the current point in the observation sequence (state $S3$), action $f$ maximizes the objective function. If the current observation is $f$ it will be flagged as the critical trigger action.