\section{Benchmark Results}
We review the overall accuracy of our algorithm in benchmark domains by evaluating how well (1) it ignores extraneous actions and (2) flags undesirable actions in a ground trace.

\subsection{Ignoring Extraneous Actions}
\label{subsec:ignoreea}
Intuitively, when encountered with extraneous actions in the trace, the algorithm must be able to avoid flagging it as critical. We define mean ignored EA\% percentage for a domain as: (sum of ignored EA\% per trial / number of trials with EA\%$>$0). Table \ref{tab:ignoredeaforlevels} shows how mean ignored EA\% varies with the noise level of the observation trace and objective weight assignments.

Results show that our algorithm performs consistently well in ignoring extraneous actions for all benchmark domains. This high accuracy rate can be attributed to our process of selecting critical trigger actions. Since candidate trigger actions are extracted from a set of alternative plans leading to the same undesirable state, the likelihood of true extraneous actions to appear in the set of alternative plans is low. This reduces the likelihood of false-positives in the trace (observed extraneous action being flagged as critical trigger).


   \begin{table*}[!t]
     \centering
   \begin{small}
	\begin{tabular}{|c||c|c|c||c|c|c|c|c|c|c|c|}
		\hline
		\multirow{3}{*}{Domain} & \multicolumn{3}{c||}{EA\% in trace} & \multicolumn{7}{c|}{OW Assignments  (Certainty $\vert$ Timeliness $\vert$ Desirability)} \\ \cline{2-11} 
		& 50\% & 75\% & 100\% & 0$\vert$0$\vert$1 & 0 $\vert$.5$\vert$.5 & 0$\vert1$$\vert$0 & .3$\vert$.3$\vert$.3 & .5$\vert$0$\vert$.5 & .5$\vert$.5$\vert$0 & 1$\vert$0$\vert$0 \\ \cline{2-11} 
		& \multicolumn{10}{c|}{Mean Ignored EA\%} \\ \hline
		\texttt{blo} & \begin{tabular}[c]{@{}c@{}}95.17 \\ (7.96)\end{tabular} & \begin{tabular}[c]{@{}c@{}}97.56\\ (3.60)\end{tabular} & \begin{tabular}[c]{@{}c@{}}97.30\\ (3.19)\end{tabular} & \begin{tabular}[c]{@{}c@{}}98.97\\ (1.58)\end{tabular} & \begin{tabular}[c]{@{}c@{}}94.69\\ (6.74)\end{tabular} & \begin{tabular}[c]{@{}c@{}}95.23\\ (6.18)\end{tabular} & \begin{tabular}[c]{@{}c@{}}95.35\\ (7.02)\end{tabular} & \begin{tabular}[c]{@{}c@{}}98.55\\ (1.71)\end{tabular} & \begin{tabular}[c]{@{}c@{}}95.39\\ (6.99)\end{tabular} & \begin{tabular}[c]{@{}c@{}}98.55\\ (1.72)\end{tabular} \\ \hline
		\texttt{ipc} & \begin{tabular}[c]{@{}c@{}}86.41\\ (14.40)\end{tabular} & \begin{tabular}[c]{@{}c@{}}89.87\\ (11.59)\end{tabular} & \begin{tabular}[c]{@{}c@{}}89.86\\ (10.65)\end{tabular} & \begin{tabular}[c]{@{}c@{}}95.65\\ (3.71)\end{tabular} & \begin{tabular}[c]{@{}c@{}}83.16\\ (12.97)\end{tabular} & \begin{tabular}[c]{@{}c@{}}81.31\\ (12.85)\end{tabular} & \begin{tabular}[c]{@{}c@{}}83.90\\ (15.08)\end{tabular} & \begin{tabular}[c]{@{}c@{}}95.93\\ (3.38)\end{tabular} & \begin{tabular}[c]{@{}c@{}}85.12\\ (14.75)\end{tabular} & \begin{tabular}[c]{@{}c@{}}95.93\\ (3.39)\end{tabular} \\ \hline
		\texttt{log} & \begin{tabular}[c]{@{}c@{}}97.93 \\ (4.39)\end{tabular} & \begin{tabular}[c]{@{}c@{}}96.72\\ (5.71)\end{tabular} & \begin{tabular}[c]{@{}c@{}}96.05\\ (5.37)\end{tabular} & \begin{tabular}[c]{@{}c@{}}98.38\\ (2.78)\end{tabular} & \begin{tabular}[c]{@{}c@{}}94.42\\ (7.16)\end{tabular} & \begin{tabular}[c]{@{}c@{}}96.10\\ (3.62)\end{tabular} & \begin{tabular}[c]{@{}c@{}}96.17\\ (6.87)\end{tabular} & \begin{tabular}[c]{@{}c@{}}98.53\\ (2.55)\end{tabular} & \begin{tabular}[c]{@{}c@{}}96.17\\ (6.87)\end{tabular} & \begin{tabular}[c]{@{}c@{}}98.53\\ (2.55)\end{tabular} \\ \hline
		\texttt{nav} & 100 (0) & 100 (0) & 100 (0) & 100 (0) & 100 (0) & 100 (0) & 100 (0) & 100 (0) & 100 (0) & 100 (0) \\ \hline
	\end{tabular}		
   \end{small}
	\caption{Mean Ignored EA\%  (and standard deviation) for benchmark domains for levels of EA\% in observation trace and objective weight assignment classes}
	\label{tab:ignoredeaforlevels}
\end{table*}

OW combinations also influence ignored EA\%. This effect was significant ($p<0.05$) for \texttt{blo}, \texttt{ipc} and \texttt{log} domains. Post-hoc analysis using TukeyHSD at $\alpha=0.05$ shows that across domains, OW combinations that do not consider the timeliness metric better perform at ignoring extraneous actions than combinations that include timeliness. Because timeliness metric captures progress yet to be made from current state to triggering the undesirable state in terms of remaining steps, in the presence of extraneous actions, which do not contribute to progress toward the undesirable state, the metric does not sufficiently demote the extraneous action in the candidate action set, leading to false-positives. In contrast, certainty and desirability metrics look for occurrences of an action in undesirable plans. As extraneous actions do not occur in undesirable plans, both metrics are capable of filtering extraneous actions from the candidate action pool by minimizing objective function value and preventing them from being selected as the critical trigger.


\subsection{Flagging Undesirable Actions}
Flag UP\% captures how well the algorithm flags observations as critical triggers given that the observation appears in an undesirable plan treated as ground truth. We first look at flagged up\% in traces with 0\% noise and full observability (i.e., best-case scenario) to establish an upper bound to the metric.

\begin{table}[!htpb]
	\centering
	\begin{tabular}{|l|c|c|c|c|}
		\hline
		\multirow{2}{*}{Domain} & \multicolumn{4}{c|}{Flagged UP\%} \\ \cline{2-5} 
		& Mean   & SD     & Min    & Max    \\ \hline
		\texttt{blo}                     & 37.12  & 24.83  & 6.12   & 77.78  \\ 
		\texttt{ipc}                     & 43.27  & 30.55  & 0.00   & 90.00  \\ 
		\texttt{log}                     & 31.77  & 17.27  & 12.82  & 66.67  \\ 
		\texttt{nav}                     & 61.68  & 40.31  & 14.10  & 100.00 \\ \hline
	\end{tabular}
		\caption{Flagged UP\% for traces with 0\% EA and 100\% PO for \texttt{Block-words} (\texttt{bw}), \texttt{IPC-grid+} (\texttt{ipc}), \texttt{Logistics} (\texttt{log}), and \texttt{Grid-Navigation}   (\texttt{nav}) domains}
		\label{tab:flagupupperbound}
\end{table}
Table \ref{tab:flagupupperbound} shows a very large range (Max-Min) for Flag UP\%. This indicates that although the only variable for this sample is OW, Flag UP\% is also sensitive to other external factors that have not been accounted for in our proposed objective function. The challenge lies in identifying these external factors and determining their relationship so that the objective function can be tuned to improve accuracy. This will be the main focus of our future work. 

Table \ref{tab:flaggedupforlevels} shows that flagged UP\% also increases when observability increases in the trace. Factor analysis shows that this positive effect is significant ($p<0.05$) for all four benchmark domains. Interestingly, high flagged UP\% was reported for OW combinations that consider timeliness: specifically for configurations, certainty-timeliness-desirability with equal weighting, certainty-timeliness with equal weighting and desirability-timeliness with equal weighting. Post-hoc analysis using TukeyHSD at $\alpha=0.05$ shows that this difference is significant. Thus, we conclude that the timeliness metric can improve the true-positive rate, yielding higher precision for the algorithm. Timeliness metric is sensitive to partial observability because it sufficiently captures distance to triggering an undesirable state, which is a consistent indicator of the progress is being made. Even with partial observability, the remaining steps of a plan change in such a way that it can be tracked and correctly reflected in the objective function. Accuracy of certainty and desirability metrics get affected amidst partial observability because they consider occurrences of an action. When observability diminishes, different alternative plans may get selected as state of the world changes in an ad-hoc manner, which affects the candidate trigger action set and the ability of the metrics to be consistent. 

\begin{table*}[!htpb]
	\centering
\begin{small}
	\begin{tabular}{|c||c|c|c|c||c|c|c|c|c|c|c|}
		\hline
		\multirow{3}{*}{Domain} & \multicolumn{4}{c||}{PO\% in trace} & \multicolumn{7}{c|}{OW Assignments (Certainty $\vert$ Timeliness $\vert$ Desirability)} \\ \cline{2-12} 
		& 25\% & 50\% & 75\% & 100\% & 0$\vert$0$\vert$1 & 0$\vert$.5$\vert$.5 & 0$\vert$1$\vert$0 & .3$\vert$.3$\vert$.3 & .5$\vert$0$\vert$.5 & .5$\vert$.5$\vert$0 & 1$\vert$0$\vert$0 \\ \cline{2-12} 
		& \multicolumn{11}{c|}{Mean flagged UP\%} \\ \hline
		\texttt{blo} & \begin{tabular}[c]{@{}c@{}}20.08\\ (21.16)\end{tabular} & \begin{tabular}[c]{@{}c@{}}23.57\\ (14.40)\end{tabular} & \begin{tabular}[c]{@{}c@{}}31.76\\ (19.25)\end{tabular} & \begin{tabular}[c]{@{}c@{}}40.19\\ (26.09)\end{tabular} & \begin{tabular}[c]{@{}c@{}}9.22\\ (7.62)\end{tabular} & \begin{tabular}[c]{@{}c@{}}24.75\\ (20.12)\end{tabular} & \begin{tabular}[c]{@{}c@{}}37.01\\ (17.53)\end{tabular} & \begin{tabular}[c]{@{}c@{}}46.48\\ (24.85)\end{tabular} & \begin{tabular}[c]{@{}c@{}}19.03\\ (7.56)\end{tabular} & \begin{tabular}[c]{@{}c@{}}46.79\\ (24.89)\end{tabular} & \begin{tabular}[c]{@{}c@{}}19.03\\ (7.56)\end{tabular} \\ \hline
		\texttt{ipc} & \begin{tabular}[c]{@{}c@{}}6.88\\ (18.82)\end{tabular} & \begin{tabular}[c]{@{}c@{}}25.64\\ (20.04)\end{tabular} & \begin{tabular}[c]{@{}c@{}}42.27\\ (32.04)\end{tabular} & \begin{tabular}[c]{@{}c@{}}50.77\\ (36.22)\end{tabular} & \begin{tabular}[c]{@{}c@{}}9.70\\ (5.19)\end{tabular} & \begin{tabular}[c]{@{}c@{}}46.46\\ (34.74)\end{tabular} & \begin{tabular}[c]{@{}c@{}}47.86\\ (35.02)\end{tabular} & \begin{tabular}[c]{@{}c@{}}47.52\\ (35.76)\end{tabular} & \begin{tabular}[c]{@{}c@{}}10.57\\ (4.91)\end{tabular} & \begin{tabular}[c]{@{}c@{}}47.05\\ (35.93)\end{tabular} & \begin{tabular}[c]{@{}c@{}}10.57\\ (4.91)\end{tabular} \\ \hline
		\texttt{log} & \begin{tabular}[c]{@{}c@{}}18.37\\ (27.43)\end{tabular} & \begin{tabular}[c]{@{}c@{}}16.40\\ (20.09)\end{tabular} & \begin{tabular}[c]{@{}c@{}}20.41\\ (19.48)\end{tabular} & \begin{tabular}[c]{@{}c@{}}31.54\\ (17.83)\end{tabular} & \begin{tabular}[c]{@{}c@{}}15.19\\ (10.61)\end{tabular} & \begin{tabular}[c]{@{}c@{}}20.91\\ (28.17)\end{tabular} & \begin{tabular}[c]{@{}c@{}}24.59\\ (25.37)\end{tabular} & \begin{tabular}[c]{@{}c@{}}27.20\\ (28.47)\end{tabular} & \begin{tabular}[c]{@{}c@{}}18.34\\ (10.08)\end{tabular} & \begin{tabular}[c]{@{}c@{}}27.20\\ (28.47)\end{tabular} & \begin{tabular}[c]{@{}c@{}}18.34\\ (10.87)\end{tabular} \\ \hline
		\texttt{nav} & \begin{tabular}[c]{@{}c@{}}14.75\\ (19.13)\end{tabular} & \begin{tabular}[c]{@{}c@{}}32.36\\ (22.56)\end{tabular} & \begin{tabular}[c]{@{}c@{}}45.20\\ (27.38)\end{tabular} & \begin{tabular}[c]{@{}c@{}}61.67\\ (39.76)\end{tabular} & \begin{tabular}[c]{@{}c@{}}14.08\\ (3.43)\end{tabular} & \begin{tabular}[c]{@{}c@{}}56.81\\ (33.68)\end{tabular} & \begin{tabular}[c]{@{}c@{}}56.81\\ (33.68)\end{tabular} & \begin{tabular}[c]{@{}c@{}}56.81\\ (33.68)\end{tabular} & \begin{tabular}[c]{@{}c@{}}14.08\\ (3.43)\end{tabular} & \begin{tabular}[c]{@{}c@{}}56.81\\ (33.68)\end{tabular} & \begin{tabular}[c]{@{}c@{}}14.08\\ (3.43)\end{tabular} \\ \hline
	\end{tabular}
\end{small}
	\caption{Mean Flagged UP\%  (and standard deviation) for benchmark domains for levels of PO\% in observation trace and objective weight assignment classes}
	\label{tab:flaggedupforlevels}
\end{table*}
