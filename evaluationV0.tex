\section{Evaluation}
In this and the following section, we evaluate our approach and examine which factors impact the performance of detecting critical trigger actions. Table~\ref{tab:exp} summarizes our experimental parameters. Since the algorithm relies on an objective function to guide the ranking, the factors and the weights in the objective function provide a mechanism for tuning the algorithm to fit domain-specific priorities. Currently, objective weights, which are provided manually, vary between a single metric, pairs of metrics and all three metrics with equal weights. We decided on these objective weight settings in order to identify which metrics (or their combinations) are sensitive to partial observability and extraneous actions separately. Once the strengths of each metric have been identified, in terms of dealing with noise and missing actions in the observation trace, the weight assignments to improve accuracy can also be learned using machine learning algorithms.


We begin with results from a user study on cybersecurity that motivated our work. For traces from the user study, we examine the impact of the objective weights (OW) used for in the objective function. We then generalize our results to a set of benchmark domains from \cite{ramirez2009plan}. However, in the benchmark study we have more experimental control, so we also consider the impact of extraneous actions (EA) and partial observability (PO) on performance. Each of these complicates the task of mapping observations to expectations of planned actions. Thus, we expect some degradation in performance as the noise increases and observability decreases; the question is by how much? 


\begin{table}[!htpb]
  \centering
  \begin{tabular}{{|l|p{1.6in}|}}
\hline
    Variable & Settings \\
\hline
Objective weights (OW) & (1,0,0), (0,1,0), (0,0,1), \\
                       &  (0.33,0.33,0.33), (0.5,0.5,0), \\
                       & (0.5,0,0.5), (0,0.5,0.5) \\
\hline
Planning domains (PD) & {\footnotesize {\tt blocks-words, navigator, 
    ipc-grid+,  logistics}} \\
Extraneous actions (EA) & 0, 50\%, 75\%, 100\% \\
Partial observability (PO) & 25\%, 50\%, 75\%, 100\% \\
\hline
  \end{tabular}
  \caption{Independent variables for evaluating impact of algorithm parameters and sensitivity to noisy data}
  \label{tab:exp}
\end{table}

%\debug{SACH}{from icaps: \textit{\textbf{Furthermore, I am not completely convinced that these metrics (as a linear combination) can capture a critical trigger action. The objectives ''certainty” and ''desirability” are more or less capturing the same idea, both are capturing how many times an action appears either in terms of count of plans or in terms of plan lengths. Also, the objective weights provided in the setting, seem to be manually provided. Why did the authors settle on those particular values among myriads of different combinations. It would be good to get a justification about the chosen objective weights. Such weights can also be computed (or “learned”) using some machine learning techniques. Also apart from linear combination, were any non linear combinations tried out?}}}

\subsection{Dependent Variables}
We measure accuracy and computational overhead measured in CPU time to assess the performance of our algorithm on cyber-security domain (PAG) and four benchmark domains from the literature. For the cyber-security domain, accuracy is defined as the percent true-positives. For the benchmarks, we report accuracy in terms of percent true-positives and true-negatives. Computation time is included because ultimately we envision this being one component of a user supporting agent, which will require fast response. CPU time includes the time required to process each observation (one cycle of the process depicted in Figure~\ref{fig:components}) within a trial; this includes both generating the alternate plans and ranking the actions. 

We treat the undesirable plan (UP) achieving the undesirable goal state as the `ground truth' trace. This is a fair assumption because our trace generation algorithm produces traces leading to the undesirable state (with varying levels of noise and observability). We measure accuracy with two metrics: (1) success rate in ignoring extraneous actions (ignored EA) and (2) success rate in flagging undesirable actions that appear in the ground truth plan (UP) (flagged UP). Ignored EA for an observation trace is computed as the count of instances where the observation was an extraneous action and it was not flagged as a critical trigger (count EA). Thus, ignored ea\% = (count EA / number of extraneous observations in trace). Flagged UP for an observation trace is computed as the count of instances where the observation was an action from UP and was flagged as a critical trigger (count UP). Thus, flagged UP\% = (count UP / number of actions of UP that are in trace)




%put in PAG domain descrpition  + results here.
\subsection{Computer Security Domain (PAG)}
\input{pag-descriptionV0}
\subsubsection{PAG Results}
\input{user-study-dataV0}

\subsection{Benchmark Domains}
To generalize our results, we examine four domains from \cite{ramirez2009plan}. {\tt Block-Words} constructs one of 20 English words using 8 distinct letters. {\tt Grid-Navigation} forms paths through a connected graph to specific locations (goals). {\tt IPC-grid+} is a grid navigation with keys added at a restricted set of locations. {\tt Logistics} moves packages between locations. To keep the scale similar to our user study, we randomly selected four problems from each domain distribution.

\subsubsection{Trace Generation}
To construct problem factors (EA, PO), we consider four problems from each benchmark domain. 
%Our problem definition assumes observation traces are imperfect due to including actions that may not be contributing to the undesirable state (extraneous actions) and not including actions that do contribute (partial observability). 
For each problem, we generate noisy traces by incrementally building the plan starting from the initial state and interleaving it with actions from a set of precomputed extraneous actions when the current state meets the preconditions of the extraneous action. Thus, the trace generation algorithm consists of two stages (1) computing the set of extraneous actions (EA) and (2) interleaving these extraneous actions.  Actions from the original undesirable plan are randomly removed from the interleaved trace to account for partial observability (PO).

We use Metric-FF \cite{hoffman2003ff} planner to generate extraneous actions because, the planner's implementation offers configuration options to extract the relaxed planning graph and add/delete effects of actions. The challenge in generating extraneous actions is that it must be ensured that the action is truly extraneous, i.e., follows conditions (1) do not interfere with the progression of the undesirable plan  or (2) undo the progress made by the plan thus far. %Information on relaxed planning graph, and add/delete effect extracted from Metric-FF were heavily utilized in extracting true extraneous actions from the domain.% 
We incrementally build the set of extraneous action starting from selecting actions that can be executed immediately after the goal state has been reached. 
%\fromsachini{trace generation process is referred to in dependant variable section..may not be able to completely omit}
%\fromsachini{e.g. following the trace with extraneous actions I should eventually get to the undesirable state. do not delete preconditions for the next step of the undesirable plan}
%\fromsachini{e.g., in blocks words after the final block is stacked to create a word, picking up the same final block from the stack}
%\frommak{Add the detail back in if there is space...}
%\begin{comment}
%For each problem in a domain:
%\begin{smallenum}
%	\item Compute the undesirable plan $UP$ using Metric-FF
%	\item For each action $a$ in $UP$ :\\
%			 incrementally modify initial state by adding, removing add and delete effects of $a$ respectively
%	\item Return state $S_{goal}$ after all actions in $UP$ have completed; 
%	\item Select action ($a_{ex}$) from planning graph such that :\\
%			$a_{ex}$ is not in $UP$\\
%			preconditions of $a_{ex}$ are in $S_{goal}$\\
%			effects of $a_{ex}$ do not remove predicates in $S_{goal}$
%	\item Return extraneous actions $EX_{goal}$
%\end{smallenum}
%$EX_{goal}$ can be extended by modifying $S_{goal}$ with effects of a selected action from $EX_{goal}$, by following steps 4 through 5. 
%The intuition is that once an extraneous action is executed at , the resulting state may enable additional extraneous actions. Repeat execution of this process generates a tree structure of extraneous actions with an action from $EX_{goal}$ as root. 
By adding new actions, object types, and predicates to the domain model, size of the resulting extraneous action set can be significantly increased to handle longer plans and create lengthy traces. The advantage of iteratively forward-expanding the set of extraneous actions starting at the goal state that at this stage the only condition we need to check is whether or not the chosen action undoes the goal state or not. 

Our trace generation algorithm builds the ground-truth plan incrementally from the initial state, while interleaving it with extraneous actions at relevant states. The challenge is to design an algorithm that can incrementally build the undesirable plan and add extraneous actions in between such that conditions (1) and (2) are both preserved at each step. Although, the plan graph provides a set of applicable actions at each level, building the observation trace by simply selecting applicable actions at each level may not be enough because it allows for no-ops. Additionally, alternative actions available at the current level of the plan graph do not guarantee that they will not violate conditions (1) and (2) at a later state. In certain occasions this could lead to traces where the same action is being done and undone repeatedly without making progress through the plan. Our process overcomes this challenge.

% the plan graph do not guarantee selecting available alternatives action from the plan graph at each step, offers us little information about whether the selected action will affect the progress of the plan at a later stage or not. In certain occasions this could lead to traces where the same action is being done and undone repeatedly without making progress through the plan. Therefore, the challenge is to design an algorithm that can incrementally build the undesirable plan, adding extraneous actions in between such that conditions (1) and (2) are both preserved at each step. 

We define percent extraneous actions (EA) in the observation trace (number of extraneous actions in trace / number of actions in plan) as a proxy for signal-to-noise ratio $[$0, 50, 75, 100$]$. Partial observability (PO) is computed post-hoc: actions from the original plan are randomly selected and removed such that, PO\% (number of original actions in trace after removal / number of original actions in trace before removal) is one of $[$25, 50, 75, 100$]$. Starting from the initial state, actions in undesirable plan are added to the observation trace sequentially. As global state is updated with each addition, we select extraneous actions (if available) from a precomputed pool such that the selected action's preconditions are satisfied in the global state and add it to the trace, while maintaining the EA\% in the trace. Once the full trace is built, actions from the original undesirable plan are randomly selected and removed to satisfy the PO\%.

%%  \begin{smallenum}
%%  	\item Compute the undesirable plan $UP$ using Metric-FF
%%  	\item Set $current \ plan$ := $UP$, $running \ EA\%$:= 0 
%%  	%\item Set $running \ EA\%$  for the trace := 0
%%  	\item For each action $a$ in $current \ plan$ :\\
%% 		 \mbox{ }- Add $a$ to trace if $a$ in $UP$ and preconditions are met in $state$\\
%% 	     \mbox{ }- Update $state$ with add/delete effects of $a$\\
%% 	     \mbox{ }- Generate undesirable plan for new $state$ and update $current \ plan$\\
%% 	     \mbox{ }- If $running EA\%$ $<$ required EA \% for the trace \\
%% 		    \mbox{ } \mbox{ }- Decide to add $ea$ biased towards required EA\%\\
%% 		    \mbox{ } \mbox{ }- If decided \\
%% 			\mbox{ } \mbox{ } \mbox{ }- Select $ea$ from extraneous action set whose preconditions have been met in $state$\\
%% 			\mbox{ } \mbox{ } \mbox{ }- Add $ea$ to trace\\
%% 		    \mbox{ } \mbox{ } \mbox{ }- Update $running EA\%$, $state$, $current \ plan$ \\
%% %		    \mbox{ } \mbox{ } \mbox{ }- Update $state$ with add/delete effects of $ea$\\
%% %		    \mbox{ } \mbox{ } \mbox{ }- Generate undesirable plan for new $state$ and update $current \ plan$\\
%% 			\mbox{ } \mbox{ }- If not decided go to step 4
%%  	\item Fix PO\% in trace
%%  \end{smallenum}
